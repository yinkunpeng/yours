ALTER TABLE `ey_users_level` MODIFY COLUMN `discount`  int(10) NULL DEFAULT 100 COMMENT '折扣率' AFTER `amount`;
UPDATE `ey_users_level` SET `discount` = '100';
INSERT INTO `ey_users_config` (`name`, `value`, `desc`, `inc_type`, `lang`, `update_time`) VALUES ('users_reg_notallow', 'www,bbs,ftp,mail,user,users,admin,administrator,eyoucms', '不允许注册的会员名', 'users', 'cn', '1547890773');